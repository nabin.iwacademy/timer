import React from "react";
import TimerDashboard from "./Components/TimerDashboard";

const App = () => {
  return (
    <div>
      <TimerDashboard />;
    </div>
  );
};
export default App;
