import React, { Component } from "react";
import EditableTimerList from "./EditableTimerList/EditableTimerList";
import ToggleableTimerForm from "./ToggleableTimerForm/ToggleableTimerForm";
import timers from "./../api/timers";

class TimerDashboard extends Component {
  state = {
    timers: [],
  };

  handleCreateFormSubmit = (timer) => {
    this.createTimer(timer);
  };
  componentDidMount() {
    this.fetchdata();
  }

  async fetchdata() {
    const Response = await timers.get("/timers");
    this.setState({ timers: Response.data });
  }

  newTimer(attrs = {}) {
    const timer = {
      title: attrs.title || "Timer",
      project: attrs.project || "Project",
      id: (Math.random() * 100).toFixed(0),
      elapsed: 0,
    };
    return timer;
  }

  createTimer = async (timer) => {
    const t = this.newTimer(timer);
    await timers.post("/timers", t);
    this.fetchdata();
  };

  handleEditFormSubmit = (timer) => {
    this.updateTimer(timer);
  };
  updateTimer = async (attrs) => {
    // this.setState({
    //   timers: this.state.timers.map((timer) => {
    //     if (timer.id === attrs.id) {
    //       return Object.assign({}, timer, {
    //         title: attrs.title,
    //         project: attrs.project,
    //       });
    //     } else {
    //       return timer;
    //     }
    //   }),
    // });

    await timers.patch(`/timers/${attrs.id}`, {
      title: attrs.title,
      project: attrs.project,
    });
    this.fetchdata();
  };
  onDelete = (id) => {
    this.deleteTimer(id);
  };

  deleteTimer = async (timerId) => {
    // this.setState({
    //   timers: this.state.timers.filter((t) => t.id !== timerId),
    // });
    await timers.delete(`/timers/${timerId}`);
    this.fetchdata();
  };

  handleStartClick = (timerId) => {
    this.startTimer(timerId);
  };

  handleStopClick = (timerId) => {
    this.stopTimer(timerId);
  };

  async startTimer(timerId) {
    const now = Date.now();
    // this.setState({
    //   timers: this.state.timers.map((timer) => {
    //     if (timer.id === timerId) {
    //       return Object.assign({}, timer, {
    //         runningSince: now,
    //       });
    //     } else {
    //       return timer;
    //     }
    //   }),
    // });
    await timers.patch(`/timers/${timerId}`, {
      runningSince: now,
    });
    this.fetchdata();
  }

  async stopTimer(timerId) {
    const now = Date.now();
    // this.setState({
    //   timers: this.state.timers.map((timer) => {
    //     if (timer.id === timerId) {
    //       const lastElapsed = now - timer.runningSince;
    //       return Object.assign({}, timer, {
    //         elapsed: timer.elapsed + lastElapsed,
    //         runningSince: null,
    //       });
    //     } else {
    //       return timer;
    //     }
    //   }),
    // });
    await this.state.timers.map((timer) => {
      if (timer.id === timerId) {
        const lastElapsed = now - timer.runningSince;
        return timers.patch(`/timers/${timerId}`, {
          elapsed: timer.elapsed + lastElapsed,
          runningSince: null,
        });
      } else {
        return this.fetchdata();
      }
    });
  }

  render() {
    return (
      <div className="ui three column centered grid">
        <div className="column">
          <h1>Time App</h1>
          <hr></hr>
          <EditableTimerList
            timers={this.state.timers}
            onFormSubmit={this.handleEditFormSubmit}
            onDelete={this.onDelete}
            onStartClick={this.handleStartClick}
            onStopClick={this.handleStopClick}
          />
          <ToggleableTimerForm
            onFormSubmit={this.handleCreateFormSubmit}
          ></ToggleableTimerForm>
        </div>
      </div>
    );
  }
}

export default TimerDashboard;
